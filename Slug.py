import pygame
import random
import Animals


class Slug(Animals.Animals):
    def __init__(self, x, y, a):
        super().__init__(x, y, a)
        self.sprite = pygame.image.load("Resources/slug.png")
