import pygame
import Patch


def get_flower_points(hp):
    res = 0
    if hp <= 10:
        res = 0
    elif 20 < hp <= 40:
        res = 2
    elif 40 < hp <= 60:
        res = 3
    elif 60 < hp <= 80:
        res = 4
    elif hp > 80:
        res = 5
    return res


def get_hp_change(wl):
    res = 0
    if wl < 10:
        res = -5
    if 10 < wl < 30:
        res = -2
    elif 30 < wl < 50:
        res = -1
    elif 50 < wl < 90:
        res = 1
    elif wl > 90:
        res = 3
    return res


class Flower:

    def __init__(self):
        self.hp = 50
        self.water_level = 80
        self.sprite = None
        self.level = 1
        self.patch = Patch.Patch(self.level)

    def loose_water(self):
        self.water_level -= 2
        if self.water_level < 0:
            self.water_level = 0
        self.patch.patch_type = Patch.get_patch_type(self.water_level)

    def update_sprite(self, lvl):
        self.sprite = pygame.image.load("Resources/Flower"+str(lvl)+".png")

