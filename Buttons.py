import Colors
import pygame


def new_button(_display_surf, x, y, height, width, bg_color, fr_color, h_color):
    pygame.draw.rect(_display_surf, bg_color, (x, y, width, height))
    if x + width > pygame.mouse.get_pos()[0] > x and y + height > pygame.mouse.get_pos()[1] > y:
        pygame.draw.rect(_display_surf, h_color, (x + 5, y + 5, width - 10, height - 10))
        if pygame.mouse.get_pressed()[0] == 1:
            return 1
    else:
        pygame.draw.rect(_display_surf, fr_color, (x + 5, y + 5, width - 10, height - 10))


def new_button_text(_display_surf, x, y, height, width, bg_color, fr_color, h_color, text):
    pygame.draw.rect(_display_surf, bg_color, (x, y, width, height))
    if x + width > pygame.mouse.get_pos()[0] > x and y + height > pygame.mouse.get_pos()[1] > y:
        pygame.draw.rect(_display_surf, h_color, (x + 5, y + 5, width - 10, height - 10))
        if pygame.mouse.get_pressed()[0] == 1:
            return 1
    else:
        pygame.draw.rect(_display_surf, fr_color, (x + 5, y + 5, width - 10, height - 10))

    textRect = text.get_rect()
    textRect.center = ((x + (width / 2)), (y + (height / 2)))
    _display_surf.blit(text, textRect)


def new_arrow_button(_view, x, y, h, w):
    colors = Colors.Color()
    pygame.draw.rect(_view, colors.dark_brown, (x, y, w, h))
    pygame.draw.rect(_view, colors.light_brown, (x + 5, y + 5, w - 10, h - 10))
    points = [(x + 5, y + h - 5), (x + w - 5, y + h - 5), (x + (w / 2), y + (h / 2) - 5)]
    pygame.draw.polygon(_view, colors.white, points)
    if x + w > pygame.mouse.get_pos()[0] > x and y + h > pygame.mouse.get_pos()[1] > y:
        pygame.draw.rect(_view, colors.yellow, (x + 5, y + 5, w - 10, h - 10))
        if pygame.mouse.get_pressed()[0] == 1:
            return 1
