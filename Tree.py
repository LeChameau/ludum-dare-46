import pygame

class Tree:
    def __init__(self, x, y):
        self.x, self.y = x, y
        self.tree_sprite = pygame.image.load("Resources/tree.png")

    def draw_tree(self, view):
        view.blit(self.tree_sprite, (self.x, self.y))
