class Color:
    def __init__(self):
        self.white = (255, 255, 255)
        self.black = (0, 0, 0)
        self.grey = (128, 128, 128)
        self.light_brown = (204, 102, 0)
        self.dark_brown = (102, 51, 0)
        self.gold = (255, 175, 0)
        self.yellow = (255, 255, 0)
