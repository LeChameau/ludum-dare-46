import pygame


class Animals:

    def __init__(self,x,y,a):
        self.x = x
        self.y = y
        self.hp = 10
        self.attack = a
        self.sprite = None

    def hit(self, target):
        if target is not None:
            target.hp -= self.attack

    def draw(self, view):
        view.blit(self.sprite, (self.x, self.y))
