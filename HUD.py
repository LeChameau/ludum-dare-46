import pygame
import Colors
import Buttons


class HUD:
    def __init__(self):
        self.flower_points_sprite = pygame.image.load("Resources/fp.png").convert()
        self.health_points_sprite = pygame.image.load("Resources/hp.png").convert()
        self.water_level_sprite = pygame.image.load("Resources/wl.png").convert()
        self.watering_can_sprite = pygame.image.load("Resources/watering_can.png")
        self.level_up_sprite = pygame.image.load("Resources/levelup.png")
        self.angry_bird_sprite = pygame.image.load("Resources/call_the_police.png")
        self.font = pygame.font.SysFont('Arial', 22)
        self.font2 = pygame.font.SysFont('Arial', 15)
        self.color = Colors.Color()
        self.x1 = None
        self.y1 = None
        self.w1 = None
        self.h1 = None

        self.x2 = None
        self.y2 = None
        self.w2 = None
        self.h2 = None

        self.watering_event = pygame.event.Event(pygame.USEREVENT + 1)
        self.small_hud_event = pygame.event.Event(pygame.USEREVENT + 2)
        self.level_up_event = pygame.event.Event(pygame.USEREVENT + 3)
        self.buy_angry_bird = pygame.event.Event(pygame.USEREVENT + 4)

    def new_hud(self, view, x, y, w, h):
        self.x1, self.y1, self.w1, self.h1 = x, y, w, h
        pygame.draw.rect(view, self.color.dark_brown, (self.x1, self.y1, self.w1, self.h1))
        pygame.draw.rect(view, self.color.gold, (self.x1, self.y1, self.w1 - 10, self.h1 - 10))
        pygame.draw.rect(view, self.color.dark_brown, (self.x1, self.y1, 800, self.h1))
        pygame.draw.rect(view, self.color.gold, (self.x1 + self.w1, self.y1 + 5, 795 - self.w1, self.h1 - 10))
        self.new_large_hud(view, self.x1 + self.w1, self.y1, 1024 - self.w1, self.h1)

    def new_large_hud(self, view, x, y, w, h):
        self.x2, self.y2, self.w2, self.h2 = x, y, w, h
        pygame.draw.rect(view, self.color.dark_brown, (self.x2, self.y2, self.w2, self.h2))
        pygame.draw.rect(view, self.color.gold, (self.x2 + 5, self.y2 + 5, self.w2 - 10, self.h2 - 10))
        if Buttons.new_button(view, self.x2 + 20, self.y2 + 85, 30, 70,
                              self.color.dark_brown, self.color.light_brown, self.color.yellow):
            pygame.event.post(self.watering_event)
        string_show = self.font2.render("Buy for 20", True, self.color.black)
        view.blit(string_show, (self.x2 + 25, self.y2 + 90))
        if Buttons.new_button_text(view, self.x2 + 100, self.y2 + 85, 30, 70,self.color.dark_brown, self.color.light_brown,
                                   self.color.yellow, self.font2.render("Buy for 120", True, self.color.black)):
            pygame.event.post(self.level_up_event)
        if Buttons.new_button(view, self.x2 + 200, self.y2 + 85, 30, 70,
                              self.color.dark_brown, self.color.light_brown, self.color.yellow):
            pygame.event.post(self.buy_angry_bird)
        string_show = self.font2.render("Buy for 50", True, self.color.black)
        view.blit(string_show, (self.x2 + 205, self.y2 + 90))
        self.draw_watering_can(view)
        self.draw_levelup(view)
        self.draw_angry_bird(view)

    def update_small_hud(self, view, hp, wl, fp, font):
        pygame.draw.rect(view, self.color.dark_brown, (self.x1, self.y1, self.w1, self.h1))
        pygame.draw.rect(view, self.color.gold, (self.x1 + 5, self.y1 + 5, self.w1 - 10, self.h1 - 10))
        view.blit(self.health_points_sprite, (self.x1 + 7, self.y1 + 7))
        view.blit(self.flower_points_sprite, (self.x1 + 7, self.y1 + 42))
        view.blit(self.water_level_sprite, (self.x1 + 7, self.y1 + 77))
        string_show = (self.font.render(str(hp), True, self.color.black))
        view.blit(string_show, (45, self.y1 + 7))
        string_show = (self.font.render(str(fp), True, self.color.black))
        view.blit(string_show, (45, self.y1 + 42))
        string_show = (self.font.render(str(wl), True, self.color.black))
        view.blit(string_show, (45, self.y1 + 77))

    def draw_watering_can(self, view):
        view.blit(self.watering_can_sprite, (self.x2 + 20, self.y2 + 10))

    def draw_levelup(self, view):
        view.blit(self.level_up_sprite, (self.x2 + 100, self.y2 + 10))

    def draw_angry_bird(self, view):
        view.blit(self.angry_bird_sprite, (self.x2+200, self.y2 + 10))

    def draw_angry_bird_ext(self, view, x, y):
        view.blit(self.angry_bird_sprite, (x, y))


