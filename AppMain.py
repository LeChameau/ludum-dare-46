import pygame
import random
import datetime
import Flower
import Colors
import HUD
import Tree
import Slug
import Angry_bird
import Buttons


class App:
    width = 1024
    height = 768
    FlowerPoints = None
    game_name = "LD44"
    clock = pygame.time.Clock()

    def __init__(self):
        self._running = True
        self._view = None
        self.Font = None
        self.flower_points = None
        self.flower = None
        self.HUD = None
        self.last_sec = None
        self.bg = None
        self.tree = None
        self.Colors = Colors.Color()
        self.update_view = False
        self.current_level = 1
        self.in_edit = False
        self.animals = list()
        self.win = None

        self.on_init()

    def on_init(self):
        pygame.init()
        pygame.display.set_caption("Flora is chilling")
        self.screen_initialisation()
        self.flower = Flower.Flower()
        self.flower.update_sprite(self.current_level)
        self.flower_init()
        self.flower_points = 0
        self.tree_init()
        self.tree.draw_tree(self._view)
        self._running = True
        self.intro()

    def intro(self):
        intro = True
        while intro:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
            self._view.blit(self.bg, (0, 0))
            TitleText = pygame.font.SysFont('Arial ', 60)
            creditsText = pygame.font.SysFont('Arial', 30)

            string_show = TitleText.render("Flora is chilling", True, self.Colors.white)
            self._view.blit(creditsText.render("by TheShamo, Compo - Ludum Dare 46", True, self.Colors.black),
                            (0, self.height - 30))

            if Buttons.new_button_text(self._view, self.width / 2 - 200, self.height / 4 - 100, 200, 400,
                                       self.Colors.dark_brown, self.Colors.light_brown, self.Colors.gold, string_show):
                intro = False
                self.on_execute()

            pygame.display.update()
            self.clock.tick(15)

    def on_execute(self):
        while self._running:
            for event in pygame.event.get():
                self.on_event(event)
            if self.last_sec != datetime.datetime.now().second:
                self.on_loop()
                self.last_sec = datetime.datetime.now().second
                self.on_render()
                self.in_edit = False
        self.outro()

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self.on_cleanup()
        if event.type == self.HUD.watering_event.type and not self.in_edit:
            self.in_edit = True
            if self.flower_points >= 20:
                self.watering()
                self.flower_points -= 20
        if event.type == self.HUD.level_up_event.type and not self.in_edit:
            if self.flower_points >= 120:
                self.level_up()
        if event.type == self.HUD.buy_angry_bird.type and not self.in_edit:
            if self.flower_points >= 50:
                self.buying_angry_bird()
        self.HUD.new_large_hud(self._view, 100, self.height - 130, self.width - 100, 130)

    def on_render(self):
        if self.update_view:
            self._view.blit(self.bg, (0, 0))
            self.flower_init()
            self.HUD.new_large_hud(self._view, 100, self.height - 130, self.width - 100, 130)
            self.tree.draw_tree(self._view)
            for a in self.animals:
                a.draw(self._view)
            self.update_view = False
        self.HUD.update_small_hud(self._view, self.flower.hp, self.flower.water_level, self.flower_points, self.Font)
        pygame.display.flip()

    def on_loop(self):
        self.flower.loose_water()
        self.flower.hp += Flower.get_hp_change(int(self.flower.water_level))
        if self.slug_spawn():
            s = Slug.Slug(random.randint(800, 1024 - 70), random.randint(430, 768 - 200), 2)
            self.animals.append(s)
        if self.flower.hp > 100:
            self.flower.hp = 100
        if self.flower.hp < 0:
            self.flower.hp = 0
            self.loosing()
        self.flower_points += Flower.get_flower_points(int(self.flower.hp))
        self.animals_action()

    def on_cleanup(self):
        pygame.quit()

    def loosing(self):
        self.win = False
        self._running = False

    def winning(self):
        self.win = True
        self._running = False

    def watering(self):
        self.flower.water_level += 30
        if self.flower.water_level > 100:
            self.flower.water_level = 100
        self.update_view = True

    def level_up(self):
        self.current_level += 1
        self.flower.update_sprite(self.current_level)
        self.flower_points -= 120
        if self.current_level > 4:
            self.winning()
        self.update_view = True

    def buying_angry_bird(self):
        a = Angry_bird.AngryBird(200, 100 + 150 - 10, 5)
        self.animals.append(a)
        self.flower_points -= 50
        self.update_view = True

    def animals_action(self):
        for a in self.animals:
            if type(a) == Angry_bird.AngryBird:
                i = [y for y in self.animals if type(y) == Slug.Slug]
                if len(i) > 0:
                    a.hit(random.choice(i))
                    a.usage += 1
                    if a.usage == 10:
                        a.hp = 0
            elif type(a) == Slug.Slug:
                a.hit(self.flower)
                print(a.hp)
            if a.hp <= 0:
                self.animals.remove(a)
                del a
                self.update_view = True

    def slug_spawn(self):
        spawn = False
        for i in range(1, self.current_level):
            if random.randint(0, 100) < 10:
                spawn = True
        self.update_view = True
        return spawn

    def outro(self):
        outro = True
        choice = None
        while outro:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
                self._view.blit(self.bg, (0, 0))
                pygame.draw.rect(self._view, self.Colors.dark_brown, (self.width/3, self.height/6, 400, 200))
                pygame.draw.rect(self._view, self.Colors.gold, (self.width/3+ 5, self.height/6 + 5, 400 - 10, 200 - 10))

                string_show = self.Font.render("Thanks a lot for playing !", True, self.Colors.black)
                textRect = string_show.get_rect()
                textRect.center = ((self.width/3 + (200 / 2)), (self.height/6 + (400 / 2))-11)
                self._view.blit(string_show, textRect)

                string_show = self.Font.render("You can quit the game try another round !", True, self.Colors.black)
                textRect = string_show.get_rect()
                textRect.center = ((self.width / 3 + (200 / 2)), (self.height / 6 + (400 / 2))+11)
                self._view.blit(string_show, textRect)

                string_show = self.Font.render("Quit", True, self.Colors.white)

                if Buttons.new_button_text(self._view, self.width / 10, 2*self.height / 3, 200, 400,
                                           self.Colors.dark_brown, self.Colors.light_brown, self.Colors.gold,
                                           string_show):
                    outro = False
                    choice = 0

                string_show = self.Font.render("Replay", True, self.Colors.white)
                if Buttons.new_button_text(self._view, 6 * self.width / 10, 2*self.height / 3, 200, 400,
                                           self.Colors.dark_brown, self.Colors.light_brown, self.Colors.gold,
                                           string_show):
                    choice = 1
                    outro = False
            pygame.display.update()
        if not choice:
            pygame.event.post(pygame.QUIT)
        else:
            self.on_init()


    # region Initialisation

    def screen_initialisation(self):
        self._view = pygame.display.set_mode((self.width, self.height), pygame.HWSURFACE)
        self.bg = pygame.image.load("Resources/Background.png").convert()
        self._view.blit(self.bg, (0, 0))
        self.HUD = HUD.HUD()
        self.HUD.new_hud(self._view, 0, self.height - 130, 100, 130)
        self.HUD.new_large_hud(self._view, 100, self.height - 130, self.width - 100, 130)
        pygame.display.set_caption(self.game_name)
        self.Font = pygame.font.SysFont('Arial', 22)

    def flower_init(self):
        self._view.blit(self.flower.patch.patch_sprite[self.flower.patch.patch_type], ((self.width / 2 - 70),
                                                                                       (self.height / 5) * 3 - 25))
        self._view.blit(self.flower.sprite, ((self.width / 2 - 70), (self.height / 5) * 3 - 100))

    def tree_init(self):
        self.tree = Tree.Tree(0, 100)


game = App()
