import pygame


def get_patch_type(a):
    res = 0
    if a <= 10:
        res = 3
    elif 10 < a <= 30:
        res = 2
    elif 30 < a <= 50:
        res = 1
    elif 50 <= a:
        res = 0
    return res


class Patch:
    def __init__(self,lvl):
        self.size = lvl
        self.patch_type = 0
        self.patch_sprite = [pygame.image.load("Resources/patch1.png"), pygame.image.load("Resources/patch2.png"),
                             pygame.image.load("Resources/patch3.png"), pygame.image.load("Resources/patch4.png")]